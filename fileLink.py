#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import urllib.parse

class FileLink():
    def __init__(self, link, url):
        self.filetype = link
        self.name = link
        self.url = url
        self.link = link

    @property
    def link(self):
        return self.__link

    @link.setter
    def link(self, link):
        # urljoin from urllib.parse should do everything to build the right link from base
        self.__link = urllib.parse.urljoin(self.url, link)
    
    @property
    def name(self):
        return self.__name
    
    @name.setter
    def name(self, link):
        self.__name = link.rsplit('/', 1)[-1]

    @property
    def filetype(self):
        return self.__filetype

    @filetype.setter
    def filetype(self, link):
        self.__filetype = link.rsplit('.', 1)[-1] 

    def checkfiletype(self, check_type):
        if self.filetype == check_type:
            return True
        else:
            return False

