
from tkinter import filedialog
from tkinter import *
from tkinter.ttk import *
import re
from pathlib import Path

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
    
    def init_window(self):
        style = Style()
        style.theme_use('clam')
        self.master.title('FIONA renamer')
        
        topFrame = Frame()
        topFrame.pack(fill=X, side=TOP, padx=30, pady=15)
        lbl = Label(topFrame, text="The selected file names are changed to the FIONA convention,\n i.e. lower case, hyphens and numbers only.")
        lbl.pack(fill=X)

        self.btnFrame = Frame()
        self.btnFrame.pack(fill=BOTH,padx=20, pady=5, side=LEFT)

        self.btnFrame.grid_columnconfigure(0, weight=0)
        self.btnFrame.grid_columnconfigure(1, weight=0)

        openFilesBtn = Button(self.btnFrame, text="Select files", command=self.select_files)

        openFilesBtn.grid(row=0, column=0, padx=5, sticky="ew")

    def select_files(self):
        self.clear_view()
        # returns a list of filenames
        self.master.filenames = filedialog.askopenfilenames(initialdir = "/home/timo/Schreibtisch", title="Select file")
        self.display_filenames()

    def display_filenames(self):
        if len(self.master.filenames) > 0:

            self.old_pathes = []
            self.new_pathes = []

            self.tableFrame = Frame(self.master)
            self.tableFrame.pack(fill=BOTH, side=RIGHT, padx=20, pady=20, expand=True)

            self.tableFrame.grid_columnconfigure(0, weight=1)
            self.tableFrame.grid_columnconfigure(1, weight=1)
            self.tableFrame.grid_rowconfigure(1, weight=1)

            self.oldNameFrame = Frame(self.tableFrame)
            self.newNameFrame = Frame(self.tableFrame)
            self.statusFrame = Frame(self.tableFrame)
            
            lbl2 = Label(self.tableFrame, text="old names")
            lbl2.grid(row=0, column=0, sticky="n")
            lbl3 = Label(self.tableFrame, text="new names")
            lbl3.grid(row=0, column=1, sticky="n")
            lbl4 = Label(self.tableFrame, text="status")
            lbl4.grid(row=0, column=2, sticky="n")

            self.oldNameFrame.grid(row=1, column=0, sticky="nswe", padx=5)
            self.newNameFrame.grid(row=1, column=1, sticky="nswe", padx=5)
            self.statusFrame.grid(row=1, column=2, sticky="nswe", padx=5)

            self.listbox_old_name = Listbox(self.oldNameFrame)
            self.listbox_new_name = Listbox(self.newNameFrame)
            self.listbox_status = Listbox(self.statusFrame)
            self.listbox_old_name.pack(side=LEFT, fill=BOTH, expand=True)
            self.listbox_new_name.pack(side=LEFT, fill=BOTH, expand=True)
            self.listbox_status.pack(side=LEFT, fill=BOTH, expand=True)

            scrollbarOldName = Scrollbar(self.tableFrame, orient="vertical", command=self.listbox_old_name.yview)
            scrollbarOldName.grid(row=1, column=3, sticky="ns")
            self.listbox_old_name.config(yscrollcommand=scrollbarOldName.set)
            self.listbox_new_name.config(yscrollcommand=scrollbarOldName.set)
            self.listbox_status.config(yscrollcommand=scrollbarOldName.set)

            for i in range(len(self.master.filenames)):
                file_name = self.master.filenames[i]
                
                self.old_pathes.append(Path(file_name))

                dirname = Path(file_name).parents[0]

                name = Path(file_name).name
                # nameLbl = Label(self.tableFrame, text=name)
                # nameLbl.grid(row=i+1, column=0, sticky="w")
                self.listbox_old_name.insert(END, name)

                newName = self.fionalize(name)
                # newNameLbl = Label(self.tableFrame, text=newName)
                # newNameLbl.grid(row=i+1, column=1, sticky="w")
                self.listbox_new_name.insert(END, newName)

                self.new_pathes.append(Path(dirname, newName))


            self.changeBtn = Button(self.btnFrame, text="Rename", state="normal", command=self.rename_files)
            self.changeBtn.grid(row=2, column=0, padx=5, pady=10, sticky="ew")

    def rename_files(self):
        for i in range(len(self.old_pathes)):
            try:
                self.rename_file(self.old_pathes[i], self.new_pathes[i])
                self.listbox_status.insert(END, "Done")
            except:
                self.listbox_status.insert(END, "ERROR")

        self.changeBtn.config(state="disabled")
                
    def rename_file(self, old_path, new_path):
        old_path.replace(new_path)

    def clear_view(self):
        try:
            self.tableFrame.pack_forget()
            self.tableFrame.destroy()
            self.changeBtn.grid_forget
            self.changeBtn.destroy()
        except AttributeError:
            pass

    def fionalize(self, string):
        newName=""
        ending=""

        regEnding = r"\.[a-zA-Z\d]*$"

        if re.search(regEnding, string) is not None:
            ending=re.search(regEnding, string).group(0)
            string=re.sub("\.[a-zA-Z\d]*$", "", string)

        for l in string:
            if re.search("[A-Z]", l):
                newName += l.lower()
                continue
            elif re.search("[a-z\d-]", l):
                newName += l
                continue
            elif re.search("[\s]", l):
                continue
            elif re.search("[\_]", l):
                newName += "-"
                continue
            elif re.search("[ä]", l):
                newName += "ae"
                continue
            elif re.search("[ö]", l):
                newName += "oe"
                continue
            elif re.search("[ü]", l):
                newName += "ue"
                continue
            else:
                continue
        return(newName + ending)

root = Tk()
root.minsize(800, 600)
root.geometry("800x600")
app = Window(root)

root.mainloop()