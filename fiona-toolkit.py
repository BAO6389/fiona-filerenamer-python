#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import ttk
from ttkthemes import ThemedTk

from renamer import Renamer
from downloader import Downloader

# TODO
# Fehler bei: https://www.desy.de/~gudrid/source/Prototypes-und.html

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()

    def init_window(self):
        self.main_window()

        self.master.title("FIONA Migration Toolkit")

    def main_window(self):
        welcomeLbl = ttk.Label(text="FIONA Tookit")
        welcomeLbl.pack()
        nb = ttk.Notebook(self.master, padding="10 30 10 10", width="800")
        nb.pack(fill="both", expand=True)
        renamer = Renamer(self.master).main_frame
        downloader = Downloader(self.master).main_frame
        nb.add(renamer, text="Rename Files", padding="10 10 10 10")
        nb.add(downloader, text="Download PDFs", padding="10 10 10 10")

root = ThemedTk(theme="arc", toplevel=False, themebg=True)
root.minsize(800, 800)

app = Window(root)

root.mainloop()
