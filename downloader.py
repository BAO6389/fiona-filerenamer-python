#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from pathlib import Path
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *

import requests
from bs4 import BeautifulSoup

from errorStack import AppError, ErrorStack
from fileLink import FileLink


class Downloader(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

        self.download_dir_str = StringVar()
        self.download_dir_str.trace("w", self.entry_update)
        self.download_url_str = StringVar()
        self.download_links = []
        self.download_info = StringVar()

        file_types_a = ['png', 'jpg', 'pdf', 'gif', 'docx', 'xlsx', 'doc', 'xls']
        string_vars = [IntVar() for i in range(len(file_types_a))]
        self.file_types = dict(zip(file_types_a, string_vars))

        self.error_stack = ErrorStack()

        self.init_window()


    def init_window(self):
        self.main_frame = Frame(self.master)
        self.main_frame.pack(fill="x", expand=True)

        self.info_frame = Frame(self.main_frame)
        self.info_frame.pack()

        self.dic_frame = LabelFrame(self.main_frame, text="1. set download directory")
        self.dic_frame.pack(fill="x", expand=False, pady="10")

        self.item_frame = LabelFrame(self.main_frame, text="2. which file types should be downloaded?")
        self.item_frame.pack(fill="x", expand=False, pady="10")
        
        i = 0
        for file_type in self.file_types:
            check_btn = Checkbutton(self.item_frame, text=file_type, variable=self.file_types[file_type])
            check_btn.grid(row=0, column=i, sticky="w")
            i += 1

        self.download_frame = LabelFrame(self.main_frame, text="3. Enter a URL and download the files")
        self.download_frame.pack(fill="x", expand=False, pady="10")

        info_lbl = Label(self.info_frame, text="Download all files that are linked on a website.")
        info_lbl.pack(padx=10, pady=10, side="left")

        error_btn = Button(self.info_frame, text="Show Error log", command= lambda: self.show_err_win(ErrorWin))
        error_btn.pack(padx=10, pady=10, side="right")

        self.download_frame.columnconfigure(0, weight=0)
        self.download_frame.columnconfigure(1, weight=1)

        self.url_lbl = Label(self.download_frame, text="URL ")
        self.url_lbl.grid(row=0, column=0, sticky="w", padx=10)

        self.url_field = Entry(self.download_frame, textvariable=self.download_url_str)
        self.url_field.grid(row=0, column=1, pady=10, padx=10, sticky="we")

        self.download_btn = Button(self.download_frame, state="disabled", text="Search for files on...", command=self.download_files)
        self.download_btn.grid(row=0, column=0, pady=10, padx=10)

        self.path_lbl= Label(self.dic_frame, text="Download directory: ")
        self.path_lbl.grid(row=1, column=0, pady=10, padx=10)

        self.path_show = Label(self.dic_frame, textvariable=self.download_dir_str)
        self.path_show.grid(row=1, column=1, pady=10, padx=10)

        self.path_btn = Button(self.dic_frame, text="Set Directory", command=self.set_download_dir)
        self.path_btn.grid(row=0, column=0, padx=10, pady=10, sticky="we")

        self.status_frame = Frame(self.main_frame)
        self.status_frame.pack(fill="both", expand=True, pady="10")

    def set_download_dir(self):
        self.download_dir = filedialog.askdirectory()
        self.download_dir_str.set(self.download_dir)
        self.download_btn.config(state="normal")

    def entry_update(self, *args):
        if self.url_field.get():
            self.download_btn.config(state="normal")
        else:
            self.download_btn.config(state="disabled")

    def clear_view(self):
        try:
            for widget in self.status_frame.winfo_children():
                widget.destroy()

            self.start_download_btn.destroy()
            self.progress.destroy()
        except AttributeError:
            pass
    
    def show_err_win(self, _class):
        self.error_win = Toplevel(self.master)
        _class(self.error_win, self.error_stack)

    def download_files(self):
        self.error_stack.clear()
        self.clear_view()
        
        if self.download_url_str:
            if hasattr(self, 'download_dir'):
                self.download_links = []
                try:
                    r = requests.get(self.download_url_str.get())
                    data = r.text
                    soup = BeautifulSoup(data, features="lxml")
                    current_link = ""
                    i = 1

                    self.init_treeview()

                    for link in soup.find_all('a'):
                        current_link = link.get('href')
                        # here happens everything with the links
                        try:
                            for file_type in self.file_types:
                                if self.file_types[file_type].get() == 1:
                                    if current_link.endswith(file_type) or current_link.endswith(file_type.upper()):
                                        filelink = FileLink(current_link, self.download_url_str.get())
                                        file_name = self.fionalize(filelink.name)

                                        self.download_links.append(filelink)
                                        self.tree.insert("", "end", text=i-1, values=(file_name, file_type, filelink.link, ""))
                                        
                                        i += 1
                        except Exception as e:
                            self.error_stack.add(e, f"Error while trying to build the file link for the link {link}")
                            continue

                    for img in soup.find_all('img'):
                        current_img = img.get('src')
                        try:
                            for file_type in self.file_types:
                                if self.file_types[file_type].get() == 1:
                                    if current_img.endswith(file_type) or current_img.endswith(file_type.upper()):
                                        filelink = FileLink(current_img, self.download_url_str.get())
                                        file_name = self.fionalize(filelink.name)

                                        self.download_links.append(filelink)
                                        self.tree.insert("", "end", text=i-1, values=(file_name, file_type, filelink.link, ""))
                                                                               
                                        i += 1
                        except Exception as e:
                            self.error_stack.add(e, f"Error while trying to build the file link for the img {link}")
                            continue


                except Exception as e:
                    self.error_stack.add(e, f"Error while trying to get a request from {self.download_url_str.get()}")
                finally:
                    if len(self.download_links) > 0:
                        self.start_download_btn = Button(self.download_frame, state="normal", text="Download files now", command=self.start_download)
                        self.start_download_btn.grid(row=1, column=0, pady=10, padx=10)

            else:
                return
        else:
            return
    
    def start_download(self):
        self.progress = Progressbar(self.master, orient = HORIZONTAL, length = 700, mode = 'determinate')
        self.progress.pack(expand=True, fill="y", pady=10, padx=10)
        i = 0
        len_downloads = len(self.download_links)
        for link in self.download_links:
            try:
                pdf_file = requests.get(link.link)
                path = Path(self.download_dir, self.fionalize(link.name))
                with open(path, 'wb') as file:
                    file.write(pdf_file.content)
                item = self.tree.get_children()[i]
                self.tree.set(item, column="status", value="Done")
                self.tree.update()

            except Exception as e:
                self.error_stack.add(e, f"Error while trying to save the file with the URL {link}")
                self.tree.set(item, column="status", value="Error")
                self.tree.update()

            i += 1
            self.progress['value'] = (i / len_downloads)*100
            self.master.update_idletasks()
        self.clear_view()

    def init_treeview(self):
        self.tree = Treeview(self.status_frame)
        self.tree["columns"] = ("file", "file_type", "url", "status")
        self.tree.column("#0", minwidth=0, width=80, stretch=NO)
        self.tree.column("file", minwidth=0, width=100)
        self.tree.column("file_type", minwidth=0, width=80, stretch=NO)
        self.tree.column("url")
        self.tree.column("status", minwidth=0, width=100, stretch=NO)

        self.tree.heading("#0", text="Number")
        self.tree.heading("file", text="File name")
        self.tree.heading("file_type", text="type")
        self.tree.heading("url", text="URL")
        self.tree.heading("status", text="Status")


        scrollbar = Scrollbar(self.status_frame, orient="vertical", command=self.tree.yview)
        self.tree.config(yscrollcommand=scrollbar.set)        
        self.tree.grid(row=0, column=0, sticky="nsew")
        scrollbar.grid(row=0, column=1, sticky="ns")

        self.status_frame.grid_columnconfigure(0, weight=1)
        self.status_frame.grid_columnconfigure(1, weight=0)

    def fionalize(self, string):
        newName=""
        ending=""

        regEnding = r"\.[a-zA-Z\d]*$"

        if re.search(regEnding, string) is not None:
            ending=re.search(regEnding, string).group(0)
            string=re.sub("\.[a-zA-Z\d]*$", "", string)

        for l in string:
            if re.search("[A-Z]", l):
                newName += l.lower()
                continue
            elif re.search("[a-z\d-]", l):
                newName += l
                continue
            elif re.search("[\s]", l):
                continue
            elif re.search("[\_]", l):
                newName += "-"
                continue
            elif re.search("[ä]", l):
                newName += "ae"
                continue
            elif re.search("[ö]", l):
                newName += "oe"
                continue
            elif re.search("[ü]", l):
                newName += "ue"
                continue
            else:
                continue
        return(newName + ending)

class ErrorWin():
    def __init__(self, master, errorStack):
        self.master = master
        self.master.minsize(400, 300)
        self.init_window()
        self.show_log(errorStack)
    
    def init_window(self):
        self.master.title("Errors")

    def show_log(self, errorStack):
        if len(errorStack.stack) > 0:
            text_w = Text(self.master)
            text_w.pack(fill="both", expand=True)
            for error in errorStack.stack:
                text_w.insert(END, f"{error.error}\n{error.name}\n------\n")

                # error_frame = Frame(self.master)
                # error_frame.pack()
                # lbl_e = Label(error_frame, text=error.error)
                # lbl_txt = Label(error_frame, text=error.name)
                # lbl_e.pack()
                # lbl_txt.pack()

        else:
            lbl = Label(self.master, text="No errors")
            lbl.pack()
