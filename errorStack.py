#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class ErrorStack():
    def __init__(self):
        self.stack = []
    
    def clear(self):
        self.stack = []
    
    def add(self, error, name):
        self.stack.append(AppError(error, name))

class AppError():
    def __init__(self, error, name):
        self.error = error
        self.name = name