#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re
from pathlib import Path
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *


class Renamer(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()
    
    def init_window(self):        
        self.main_frame = Frame(self.master)
        self.main_frame.pack(fill=X, side=TOP, padx=5, pady=5)
        lbl = Label(self.main_frame, text="The selected file names are changed to the FIONA convention, i.e. lower case, hyphens and numbers only.")
        lbl.pack(fill=X)

        self.btnFrame = Frame(self.main_frame)
        self.btnFrame.pack(fill=BOTH, pady=15)

        openFilesBtn = Button(self.btnFrame, text="Select files", command=self.select_files)

        openFilesBtn.grid(row=0, column=0, pady=5, sticky="e")

    def select_files(self):
        self.clear_view()
        # returns a list of filenames
        self.main_frame.filenames = filedialog.askopenfilenames(initialdir = "/home/timo/Schreibtisch", title="Select file")
        self.display_filenames()

    def display_filenames(self):
        if len(self.main_frame.filenames) > 0:

            self.old_pathes = []
            self.new_pathes = []

            self.tableFrame = Frame(self.main_frame)
            self.tableFrame.pack(fill=BOTH, expand=True)
            
            lb_frame = Frame(self.tableFrame)
            lb_frame.pack(fill=BOTH, expand=True)
        
            self.tree = Treeview(lb_frame)
            self.tree["columns"] = ("old", "new", "status")

            self.tree.column("#0", width=100, minwidth=50, stretch=False)
            self.tree.column("old", stretch=True)
            self.tree.column("new", stretch=True)
            self.tree.column("status", width=100, minwidth=50, stretch=False)

            self.tree.heading("#0", text="Number")
            self.tree.heading("old", text="Old name")
            self.tree.heading("new", text="New name")
            self.tree.heading("status", text="Status")
            
            self.tree.grid(row=0, column=0, sticky="nsew")

            scrollbar = Scrollbar(lb_frame, orient="vertical", command=self.tree.yview)
            scrollbar.grid(row=0, column=1, sticky="ns")
            self.tree.config(yscrollcommand=scrollbar.set)            

            for i in range(len(self.main_frame.filenames)):
                file_name = self.main_frame.filenames[i]
                
                self.old_pathes.append(Path(file_name))

                dirname = Path(file_name).parents[0]

                name = Path(file_name).name
                newName = self.fionalize(name)

                self.tree.insert("", "end", text=i, values=(name, newName, ""))
                self.new_pathes.append(Path(dirname, newName))

            self.changeBtn = Button(self.btnFrame, text="Rename", state="normal", command=self.rename_files)
            self.changeBtn.grid(row=0, column=1, padx=15, pady=5)

    def rename_files(self):
        items = self.tree.get_children()
        for i in range(len(self.old_pathes)):
            item = items[i]

            try:
                self.rename_file(self.old_pathes[i], self.new_pathes[i])
                self.tree.set(item, column="status", value="Done")
            except:
                self.tree.set(item, column="status", value="Error")

        self.changeBtn.config(state="disabled")
                
    def rename_file(self, old_path, new_path):
        old_path.replace(new_path)

    def clear_view(self):
        try:
            self.tableFrame.pack_forget()
            self.tableFrame.destroy()
            self.changeBtn.grid_forget
            self.changeBtn.destroy()
        except AttributeError:
            pass

    def fionalize(self, string):
        newName=""
        ending=""

        regEnding = r"\.[a-zA-Z\d]*$"

        if re.search(regEnding, string) is not None:
            ending=re.search(regEnding, string).group(0)
            string=re.sub(r"\.[a-zA-Z\d]*$", "", string)

        for l in string:
            if re.search(r"[A-Z]", l):
                newName += l.lower()
                continue
            elif re.search(r"[a-z\d-]", l):
                newName += l
                continue
            elif re.search(r"[\s]", l):
                continue
            elif re.search(r"[\_]", l):
                newName += "-"
                continue
            elif re.search(r"[ä]", l):
                newName += "ae"
                continue
            elif re.search(r"[ö]", l):
                newName += "oe"
                continue
            elif re.search(r"[ü]", l):
                newName += "ue"
                continue
            else:
                continue
        return(newName + ending)
